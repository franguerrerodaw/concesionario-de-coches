package concesionario;
import java.io.File;
import java.io.IOException;

import utiles.Menu;
import utiles.Teclado;

/**
 * Queremos modelar un concesionario de coches en Java. Nos limitaremos a las
 * siguientes opciones: A�adir un coche (se pedir� matricula, color y modelo),
 * Eliminar un coche (por matr�cula), mostrar un coche (por matr�cula), mostrar
 * coches (todo el concesionario)
 * 
 * @author Francisco Javier Guerrero Molina
 * 
 */
public class TestConcesionario{
	static Menu menu = new Menu("Concesionario de coches", new String[] {"Alta Coche", "Baja Coche", "Mostrar Coche", "Mostrar concesionario", "Contar coches del concesionario", "Mostrar coches de un color", "Salir" });
	private static Menu menuColores = new Menu("Colores de los coches", Color.generarOpcionesMenu());
	private static Menu menuModelos = new Menu("Modelos de los coches", Modelo.generarOpcionesMenu());
	private static Menu menuFicheros = new Menu("Men� Ficheros:", new String[]{"Nuevo", "Abrir", "Guardar", "Guardar Como", "Salir"});
	private static Menu menuGuardarCambios = new Menu("Men� Guardar", new String[]{"Guardar Cambios"});
	static Concesionario concesionario = new Concesionario();
	private static Object selectedFile;
	private static boolean setModificado;

	public static void main(String[] args) throws CocheNoExisteException, ColorNoValidoException, ModeloNoValidoException, CocheYaExisteException, MatriculaInvalidaException {
		do {
			switch (menu.gestionar()) {
			case 1:// "A�adir Coche
				annadirCoche();
				break;
			case 2:// Eliminar Coche
				eliminarCoche();
				break;
			case 3:// Obtener Coche
				getCoche();
				break;
			case 4:// Mostrar lista
				System.out.println(concesionario);
				break;
			case 5:// Contar coches
				System.out.println("N�mero de coches en el concesionario: "+ concesionario.size());
				break;
			case 6:// Mostrar coches de un color
				System.out.println(concesionario.getCochesColor(pedirColor()));
				break;
			case 7:// Gestionar ficheros
				menuFicheros();
			default:// Salir
				System.out.println("Hasta Pronto.");
				return;
			}
		} while (true);
	}
	
	/** ********** MEN� CONCESIONARIO ********** */

	private static void annadirCoche() {
		try {
			concesionario.annadir(Teclado.leerCadena("Introduce la matr�cula"),pedirColor(), pedirModelo());
			System.out.println("Coche a�adido con �xito");
		} catch (MatriculaInvalidaException e) {
			System.out.println(e.getMessage());
		} catch (ColorNoValidoException e) {
			System.out.println(e.getMessage());
		} catch (ModeloNoValidoException e) {
			System.out.println(e.getMessage());
		} catch (CocheYaExisteException e) {
			System.out.println(e.getMessage());
		}
	}
	
	private static void eliminarCoche() throws CocheNoExisteException, MatriculaInvalidaException {
		if (concesionario.eliminar(Teclado.leerCadena("Introduce la matr�cula")))
			System.out.println("Coche eliminado");
		else
			System.out.println("No se ha podido eliminar");
	}

	private static void getCoche() throws MatriculaInvalidaException, CocheNoExisteException {
		Coche coche = concesionario.get(Teclado.leerCadena("Introduce la matr�cula"));
		if (coche == null)
			System.out.println("No existe el coche en el concesionario.");
		else
			System.out.println(coche);
	}

	private static Modelo pedirModelo() {
		int opcion = menuModelos.gestionar();
		Modelo[] arrModelos = Modelo.getValues();
		if (opcion == arrModelos.length + 1)
			return null;
		return arrModelos[opcion - 1];
	}

	private static Color pedirColor() {
		int opcion = menuColores.gestionar();
		Color[] arrColores = Color.getValues();
		if (opcion == arrColores.length + 1)
			return null;
		return arrColores[opcion - 1];
	}
	
	/** ********** MEN� FICHEROS ********** */
	
	private static void menuFicheros() {
		 switch (menuFicheros.gestionar()) {
	        case 1:// Nuevo
	            nuevo();
	            break;
	        case 2:// Abrir
	            abrir();
	            break;
	        case 3:// Guardar
	            guardar();
	            break;
	        case 4:// Guardar como
	            guardarComo();
	            break;
	        default:// Salir
	            break;
	        }
	}
	
	/**
     * Crea un concesinario nuevo. En caso de ser modificado
     */
    private static void nuevo() {
        guardarSiModificado(); //guarda el concesionario si se ha modificado.
        inicializar();
        setSelectedFile(null);
    }

    private static void setSelectedFile(Object object) {
		selectedFile = object;
	}

	/**
     * Parte de un concesionario nuevo
     */
    public static void inicializar() {
        setModificado(false);
        setConcesionario(new Concesionario());
    }

	private static void setModificado(boolean b) {
		setModificado = b;
	}
	
	private static void setConcesionario(Concesionario concesionario) {
		ConcesionarioGUI = concesionario;
	}

	/**
     * Guarda solicitando el nombre del fichero. Se confirma la sobreescitura en
     * caso de existir el fichero.
     */
    private static void guardarComo() {
        try {
            File file = new File(
                    Teclado.leerCadena("Introduce el nombre del fichero a guardar: "));
            file = Ficheros.annadirExtension(file);
            if (Ficheros.confirmarExistencia(file)) {
                char caracter = Teclado.leerCaracter("El fichero ya existe. �Desea sobreescribirlo? (s/n)");
                switch (caracter) {
                case 'n':
                case 'N':
                    return;
                }
            }

            Ficheros.guardar(file, concesionario);
            setModificado(false);
            setSelectedFile(file);
        } catch (IOException e) {
            System.out.println("El sistema no puede guardar el fichero.");
        }
    }

    /**
     * Guarda en el fichero previo. S�lo si no hay fichero previo se solicita el
     * nombre
     */
    private static void guardar() {
        if (getSelectedFile() == null)
            guardarComo();
        else {
            try {
                Ficheros.guardar(getSelectedFile(), concesionario);
                setModificado(false);
            } catch (IOException e) {
                System.out.println("El sistema no puede guardar el fichero.");
            }
        }
    }

    private static File getSelectedFile() {
		return null;
	}

	/**
     * Recupera el contenido de un fichero. En caso de estar modificado el
     * concesionario se pregunta si desea guardarse.
     */
    private static void abrir() {
        guardarSiModificado();
        try {
            File file = new File(
                    Teclado.leerCadena("Introduce el nombre del fichero a abrir: "));
            file = Ficheros.annadirExtension(file);
            concesionario = Ficheros.leer(file);
            setSelectedFile(file);
        } catch (ClassNotFoundException e) {
            System.out
                    .println("Fichero con informaci�n distinta a la esperada.");
        } catch (IOException e) {
            System.out.println("El sistema no puede abrir el fichero.");
        }
    }

    /**
     * Se guarda el concesionario si se ha modificado y el usuario lo desea.
     * 
     * @return true si se ha guardado. false si no se ha guardado.
     */
    private static boolean guardarSiModificado() {
        if (isModificado()) {
            switch (menuGuardarCambios.gestionar()) {
            case 1:// SI
                guardar();
                return true;
            }
        }
        return false;
    }
	
	
}
