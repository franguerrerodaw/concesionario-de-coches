package concesionario;
import utiles.MenuConcesionario;
import utiles.Teclado;
/**
 * @author Francisco Javier Guerrero Molina
 */
public class TestCoche {
	static MenuConcesionario menu1 = new MenuConcesionario("Concesionario", new String[]{"A�adir coche", "Eliminar coche", "Mostrar un coche", "Mostrar concesionario", "Contar Coches", "Mostrar coches de un color", "Salir"});
	static MenuConcesionario menu2 = new MenuConcesionario("Elige un color", new String[]{"Plata", "Rojo", "Azul"});
	static MenuConcesionario menu3 = new MenuConcesionario("Elige un modelo", new String[]{"C�rdoba", "Ibiza", "Toledo", "Serie 1", "Serie 2", "Serie 3", "Serie 5"});
	static Concesionario concesionario = new Concesionario();
	
	public static void main(String[] args) {
		mostrarMenu();

	}

	private static void mostrarMenu() {
		do {
			switch (menu1.mostrar()) {
			case 1:
				//a�adir coche.
				anadirCoche();
				break;
			case 2:
				//eliminar coche.
				eliminarCoche();
				break;
			case 3:
				//mostrar un coche.
				mostrarCoche();
				break;
			case 4:
				//mostrar concesionario
				mostrarConcesionario();
				break;
			case 5:
				//contar coches
				contarCoches();
				break;
			case 6:
				//mostrar coches de un color
				System.out.println(concesionario.mostrarColor(pedirColor()));
				break;
			case 7:
				//Salir
				System.out.println("El programa ha finalizado.");
				return;
			default:
				break;
			}
		} while (true);
		
	}

	private static Color pedirColor() {
		switch (menu2.mostrar()){
			case 1:
				return Color.PLATA;
			case 2:
				return Color.ROJO;
			case 3:
				return Color.AZUL;
		}
		return null;
	}
	
	private static Modelo pedirModelo() {
		switch (menu3.mostrar()){
			case 1:
				return Modelo.CORDOBA;
			case 2:
				return Modelo.TOLEDO;
			case 3:
				return Modelo.IBIZA;
			case 4:
				return Modelo.SERIE1;
			case 5:
				return Modelo.SERIE2;
			case 6:
				return Modelo.SERIE3;
			case 7:
				return Modelo.SERIE5;
			default:
				break;
		}
	return null;
	}
	
	private static void anadirCoche() {
		if (concesionario.anadirCoche(Teclado.leerCadena("Introduce la matr�cula"), pedirColor(), pedirModelo()))
			System.out.println("Coche a�adido con �xito");
		else
			System.out.println("No se ha podido a�adir");
	}
	
	private static void eliminarCoche() {
		if (concesionario.eliminarCoche(Teclado.leerCadena("Introduce la matr�cula: ")))
			System.out.println("El coche se ha eliminado correctamente");
		else
			System.out.println("El coche no ha sido eliminado");
	}

	private static void mostrarCoche() {
		Coche coche = concesionario.mostrarCoche(Teclado.leerCadena("Introduce la matr�cula del coche a buscar: "));
		if (coche == null)
			System.out.println("El coche no existe.");
		else
			System.out.println(coche);
	}
	
	private static void mostrarConcesionario() {
		if(concesionario == null)
			System.out.println("No hay coches en el concesionario.");
		else
			System.out.println(concesionario);
	}
	
	private static void contarCoches() {
		if(concesionario.size() == 0)
			System.out.println("No hay coches en el concesionario.");
		else
			System.out.println("Hay "+ concesionario.size() +" coches en el concesionario.");
	}

}
