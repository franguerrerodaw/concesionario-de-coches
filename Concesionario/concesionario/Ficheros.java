package concesionario;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;

/**
 * 
 * @author Francisco Javier Guerrero Molina
 * 
 */
public class Ficheros {

    /**
     * Devuelve el concesionario de coches, le�do desde un fichero.
     * 
     * @param file Fichero de donde se obtendr� el concesionario
     * @return El concesionario le�do desde el fichero
     * @throws FileNotFoundException 
     * @throws ClassNotFoundException No se encuentra la clase
     * @throws IOException Error de E/S
     * @throws FicheroCorruptoException Fichero corrupto
     */
    public static Concesionario leer(File file) throws FileNotFoundException, IOException, ClassNotFoundException{
        file = annadirExtension(file);
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new BufferedInputStream(new FileInputStream(file)))) {
            return (Concesionario) objectInputStream.readObject();
        } 
//        catch (StreamCorruptedException e) {
//            throw new FicheroCorruptoException("Fichero corrupto.");
//        }
    }

    /**
     * Guarda un concesionario en el fichero indicado.
     * 
     * @param file Fichero donde guardar el concesionario
     * @param concesionario Concesionario a guardar
     * @throws IOException Error al guardar el concesionario
     */
    public static void guardar(File file, Concesionario concesionario) throws IOException {
        file = annadirExtension(file);
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(
                new BufferedOutputStream(new FileOutputStream(file, false)))) {// no a�ade, machaca el fichero
            objectOutputStream.writeObject(concesionario);
        }
    }

    /**
     * Confirma la existencia del fichero de la aplicaci�n. Para ello le a�ade la extensi�n en caso de no tenerla.
     * 
     * @param file Fichero a comprobar
     * @return true si existe. false en otro caso.
     */
    public static boolean confirmarExistencia(File file) {
        file = annadirExtension(file);
        return file.exists();
    }

    /**
     * A�ade la extensi�n ".obj" s�lo en caso de ser necesario
     * 
     * @param file Fichero a a�adir la extensi�n
     * @return Fichero con la extensi�n ".obj"
     */
    static File annadirExtension(File file) {
        String path = file.getPath();
        if (!path.endsWith(".obj"))
            return new File(path + ".obj");
        return file;
    }
}
