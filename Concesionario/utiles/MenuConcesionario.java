package utiles;
/**
 * @author Francisco Javier Guerrero Molina
 */
public class MenuConcesionario {
	private String titulo;
	private String[] opciones;
	
	public MenuConcesionario(String titulo, String[] opciones) { //constructor
		setTitulo(titulo);
		setOpciones(opciones);
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String[] getOpciones() {
		return opciones;
	}

	public void setOpciones(String[] opciones) {
		this.opciones = opciones;
	}

	public int gestionar() {
		System.out.println(this.titulo);
		for (int i = 0; i < opciones.length; i++) {
			System.out.println(i+1 +" "+ opciones[i]);
		}
		return recogerOpcion();
	}

	private int recogerOpcion() {
		int opcion;
		do {
			System.out.println("Introduce una opci�n v�lida: ");
			opcion = Teclado.leerEntero();
		} while (opcion<1 || opcion>opciones.length);
		return opcion;
	}
	
	
	
}
